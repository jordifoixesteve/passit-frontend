# Passit Front

passit-frontend is the angular client for [Passit](https://passit.io). It includes both the web version and mobile app.

We use smart and dumb components with ngrx/store.
Read about it [here](https://gist.github.com/btroncone/a6e4347326749f938510#utilizing-container-components)

API and crypto interaction are handled by passit-sdk-js which itself uses libsodium.js.

# Passit Web

## Install Extensions

- [Chrome Web Store](https://chrome.google.com/webstore/detail/passit/pgcleadjbkbghamecomebcdakdmahkeh)
- [Firefox addon](https://app.passit.io/ext/passit.xpi)

## Start developing

1. Install docker and docker-compose
2. Run `docker-compose up`

The front end is now running on localhost:4200 and the backend docker image is running on localhost:8000

Run any one off commands like `docker-compose run --rm web ng` to execute them inside the docker container.

You may want to run the client without docker. This is helpful when writing unit tests or for quickly adding packages. If you'd like to do this:

1. Start the backend in docker `docker-compose up api`
2. Ensure node 10 and npm are installed?
3. Install node packages `npm i`
4. Run the frontend `npm start` or use any `npm run ng` command from angular-cli.
5. You can still run the backend with docker using `docker-compose up api`

**Hot Reload**

To enable webpack hot reloading run `npm run hmr` instead of start.

### Rebuilding when changing package.json

If you need to run `npm i` you'll want to instead run `docker-compose build` and `docker-compose rm -v app` to remove the old container.
This is because node_modules are kept in a docker volume.

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running storybook

We write storybook stories for presentational components instead of unit tests. To run:

`npm run storybook`

## Running end-to-end tests

End-to-end tests are run via [Protractor](http://www.protractortest.org/).

We'll run the backend in docker and the frontend without for simplicity. Ensure you run `npm` first to install local node modules.

1. Start backend `docker-compose up api`
2. Start dev server `npm start`
3. Run test `npm run e2e`

The e2e script will use the existing dev server (so you don't have to wait for it to rebuild each time)

We can also run the e2e tests against firefox, by setting the `FIREFOX` environmental variable to something and we can run them against the web extension
by setting `EXTENSION` env variable to be something (before testing against the extension you have to build it with `build:ext`.

We can also run the tests in docker:

```bash
docker-compose up -d app selenium-<chrome/firefox>
[docker-compose run app build:ext]
docker-compose run [-e FIREFOX=true] [-e EXTENSION=true] app e2e:docker
```

### end-to-end test structure and how to

We ensure that each `describe` block can be run in any order.
However each `it` step inside a block actually must run in order - as to minimize set up time which is time consuming due to expensive crypto functions being used when logging in.

- The `describe` blocks do not need to run in order.
- Each `describe` block creates a new user.
- Some `it` statements within a `describe` block depend on state from previous tests and need to run in order.

New users are created in the docker backend with random user names to prevent collisions. This means we don't have to wipe the backend between tests which also saves time (at the expense of this being slightly ugly).

## Coding Style and Linting

Follow style and design patterns as seen on the [ngrx example-app](https://github.com/ngrx/platform/tree/master/projects/example-app) and from angular-cli.

```bash
npm run lint
```

### Error handling

Error and validation can get complex. We have the backend API, js SDK, services, and effects as potential sources of errors. Here are some guidelines:

- Do use ngrx-forms's client side validation
- Only use ngrx-forms async validation for "as you type" server side validation. Do not use it for server errors.
- Services should handle errors and return an Observable. They should throw handled error messages as an error.
- Effects should connect services to the correct action response. Effects are glue code and should avoid complex logic (which should be in a service).

## Development for web extension

The web extension works in Firefox and Chrome. It reuses the same angular code but we need to build it differently.

For development:

We'll run this time without docker (though you could use docker). We want to build the app with aot and watch enabled for auto reload.
Then we'll run web-ext to launch the code as an extension in Firefox.

1. Run backend either in docker (`docker-compose up api`) or hosted service
2. `npm install` (if not done already)
3. `npm run build:ext -- --watch`
4. (new term) For Firefox `npm run ext:run` and for Chrome go to `dist/manifest.json` and change `"CI_INSERT_VERSION_HERE"` to a number. Change `"content_security_policy": "script-src 'self'; object-src 'self'",` to `"content_security_policy": "script-src 'self' 'unsafe-eval'; object-src 'self'",`. (Make sure you do not commit these changes in the manifest.json file.) Open Chrome and go to `chrome://extensions/`. Ensure Developer mode is checked. Click Load unpacked and select the dist folder that is generated by the npm run build:ext step.

Because the app shares the same code, going to routes like `/popup` actually work fine in the web version. However note when not
running as an extension you won't have access to web extension api's like the browser object.

### Autofill development

Autofill code is injected into the page. It must be compiled.

1. Remove `export` from `extension/form-fill/form-fill.ts`
2. Compile with `npm build:autofill`
3. Copy and paste the resulting js file into autofill-compiled.ts as a string.

# Mobile app

Passit's mobile app runs via Nativescript. It uses a mono-repo approach described [here](https://docs.nativescript.org/angular/code-sharing/intro)
Basically a file like something.tns.ts will replace the web version of that file.

## Install

1. Install Nativescript [See instructions for Linux](https://docs.nativescript.org/start/ns-setup-linux)
2. Ensure `tns doctor` runs.
3. Set up an emulator or physical device.

## Running

Run `tns run android --bundle` or `tns run android --hmr` for hot reloading.

** Troubleshooting **

- Delete node modules and generated resources `rm -rf hooks platforms node_modules` then `npm i`
- Force a new apk build `tns build android`

### Technologies used

- [Angular](https://angular.io/) with [TypeScript](http://www.typescriptlang.org/) and [ngrx/platform](https://github.com/ngrx/platform/)
- [NativeScript](https://www.nativescript.org/) for mobile
- [libsodium.js](https://github.com/jedisct1/libsodium.js) for crypto with good defaults
