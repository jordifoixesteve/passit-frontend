export const environment = {
  production: true,
  extension: true,
  docker: false,
  nativescript: false,
  hmr: false,
  VERSION: require("../../package.json").version
};
