export const environment = {
  production: true,
  extension: false,
  docker: false,
  nativescript: false,
  hmr: false,
  VERSION: require("../../package.json").version
};
