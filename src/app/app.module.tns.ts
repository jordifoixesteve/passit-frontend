require("nativescript-localstorage");
require("nativescript-orientation");

import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module.tns";
import { AppComponent } from "./app.component";
import { AccountModule } from "./account/account.module";
import { reducers, metaReducers } from "./app.reducers";
import { AlreadyLoggedInGuard, LoggedInGuard } from "./guards";
import { ListModule } from "./list";
import { GetConfEffects } from "./get-conf/conf.effects";
import { GetConfService } from "./get-conf/";
import { Api } from "./ngsdk/api";
import { NgPassitSDK } from "./ngsdk/sdk";
import { MoonMail } from "./account/moonmail/moonmail.service";
import { SecretService } from "./secrets/secret.service";
import { SecretEffects } from "./secrets/secret.effects";
import { GeneratorService } from "./secrets";
import { GroupModule } from "./group/group.module";
import { AppDataService } from "./shared/app-data/app-data.service";
import { MobileMenuModule } from "./mobile-menu";

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';

@NgModule({
  declarations: [AppComponent],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    AccountModule,
    ListModule,
    MobileMenuModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([SecretEffects, GetConfEffects]),
    GroupModule
  ],
  providers: [
    AlreadyLoggedInGuard,
    LoggedInGuard,
    GetConfService,
    { provide: AppDataService, useClass: AppDataService },
    { provide: Api, useClass: Api },
    { provide: NgPassitSDK, useClass: NgPassitSDK },
    { provide: MoonMail, useClass: MoonMail },
    { provide: SecretService, useClass: SecretService },
    GeneratorService
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule {}
