import { Injectable } from "@angular/core";
import { knownFolders } from "tns-core-modules/file-system";
import { ShareFile } from 'nativescript-share-file';
import { NgPassitSDK } from "../ngsdk/sdk";
import { ExporterServiceBase } from "./exporter.service.common";

@Injectable()
export class ExporterService extends ExporterServiceBase {
  HEADERS = ["name", "username", "url", "password", "extra"];

  constructor(sdk: NgPassitSDK) {
    super(sdk);
  }

  async exportSecrets(): Promise<void> {
    const secrets = await this.getSecrets();
    const csv = this.serialize(secrets);
    const tmpFolder = knownFolders.temp();
    const tmpFileName = "export.csv";
    const tmpFile = tmpFolder.getFile(tmpFileName);
    tmpFile.writeText(csv).then(() => {
        const shareFile = new ShareFile();
        shareFile.open({path: tmpFile.path});
        tmpFile.remove();
    });
  }
}

