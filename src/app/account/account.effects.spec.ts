import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { provideMockActions } from "@ngrx/effects/testing";
import { Observable, ReplaySubject } from "rxjs";

import { LogoutSuccessAction } from "./account.actions";
import { LoginEffects } from "./account.effects";
import { UserService } from "./user";
import { StoreModule, combineReducers } from "@ngrx/store";

import * as fromRoot from "../app.reducers";
import * as fromAccount from "./account.reducer";

describe("Account Effects", () => {
  let effects: LoginEffects;
  let actions: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          feature: combineReducers(fromAccount.reducers)
        })
      ],
      providers: [
        LoginEffects,
        provideMockActions(() => actions),
        {
          provide: UserService,
          useValue: jasmine.createSpyObj("UserService", ["login"])
        }
      ]
    });

    effects = TestBed.get(LoginEffects);
  });

  it("logout should remove auth localstorage", () => {
    localStorage.setItem("auth", "fake");

    actions = new ReplaySubject(1);
    (actions as any).next(new LogoutSuccessAction());

    effects.logout$.subscribe(() => {
      expect(localStorage.getItem("auth")).toBe(null);
    });
  });
});
