import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Store, select } from "@ngrx/store";

import * as fromRoot from "../../app.reducers";
import * as Account from "../account.actions";
import * as fromAccount from "../account.reducer";

@Component({
  template: `
    <login-component
      [form]="form$ | async"
      [hasLoginStarted]="hasLoginStarted$ | async"
      [hasLoginFinished]="hasLoginFinished$ | async"
      [errorMessage]="errorMessage$ | async"
      [isPopup]="isPopup"
      (login)="login()"
      (goToRegister)="goToRegister()"
      (goToResetPassword)="goToResetPassword()"
    ></login-component>
  `
})
export class LoginContainer {
  form$ = this.store.pipe(select(fromAccount.getLoginForm));
  errorMessage$ = this.store.pipe(select(fromAccount.getLoginErrorMessage));
  hasLoginStarted$ = this.store.pipe(select(fromAccount.getLoginHasStarted));
  hasLoginFinished$ = this.store.pipe(select(fromAccount.getLoginHasFinished));
  isPopup = false;

  constructor(private router: Router, private store: Store<fromRoot.IState>) {
    store
      .pipe(select(fromRoot.getIsPopup))
      .subscribe(isPopup => (this.isPopup = isPopup));
  }

  goToRegister() {
    if (this.isPopup) {
      browser.tabs.create({
        url: "/index.html#/register"
      });
      window.close();
    } else {
      this.router.navigate(["/register"]);
    }
  }

  goToResetPassword() {
    if (this.isPopup) {
      browser.tabs.create({
        url: "/index.html#/reset-password"
      });
      window.close();
    } else {
      this.router.navigate(["/reset-password"]);
    }
  }

  login() {
    this.store.dispatch(new Account.LoginAction());
  }
}
