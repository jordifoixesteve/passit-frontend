import { Action } from "@ngrx/store";

export enum SetPasswordActionTypes {
  SET_PASSWORD = "[Set Password] Set Password",
  SET_PASSWORD_SUCCESS = "[Set Password] Set Password Success",
  SET_PASSWORD_FAILURE = "[Set Password] Set Password Failure",
  FORCE_SET_PASSWORD = "[Set Password] Force Set Password"
}

export class SetPassword implements Action {
  readonly type = SetPasswordActionTypes.SET_PASSWORD;
}

export class SetPasswordSuccess implements Action {
  readonly type = SetPasswordActionTypes.SET_PASSWORD_SUCCESS;
}

export class SetPasswordFailure implements Action {
  readonly type = SetPasswordActionTypes.SET_PASSWORD_FAILURE;
}

export class ForceSetPassword implements Action {
  readonly type = SetPasswordActionTypes.FORCE_SET_PASSWORD;
}

export type SetPasswordActionsUnion =
  | SetPassword
  | SetPasswordSuccess
  | SetPasswordFailure
  | ForceSetPassword;
