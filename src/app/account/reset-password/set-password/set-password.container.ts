import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import * as fromAccount from "../../account.reducer";
import * as fromSetPassword from "./set-password.reducer";
import { Store, select } from "@ngrx/store";
import { SetPassword, ForceSetPassword } from "./set-password.actions";
import { SetValueAction } from "ngrx-forms";
import { LogoutSuccessAction } from "../../account.actions";

@Component({
  template: `
    <app-set-password
      [form]="form$ | async"
      [hasStarted]="hasStarted$ | async"
      (toggleConfirm)="toggleConfirm()"
      (setPassword)="setPassword()"
      (backToLogin)="backToLogin()"
    ></app-set-password>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetPasswordContainer implements OnInit {
  form$ = this.store.pipe(select(fromAccount.getSetPasswordForm));
  hasStarted$ = this.store.pipe(select(fromAccount.getSetPasswordHasStarted));
  showConfirm: boolean;

  constructor(private store: Store<fromAccount.IAuthState>) {
    this.form$.subscribe(
      form => (this.showConfirm = form.controls.showConfirm.value)
    );
  }

  setPassword() {
    this.store.dispatch(new SetPassword());
  }

  toggleConfirm() {
    this.store.dispatch(
      new SetValueAction(
        fromSetPassword.FORM_ID + ".showConfirm",
        !this.showConfirm
      )
    );
  }

  backToLogin() {
    this.store.dispatch(new LogoutSuccessAction());
  }

  ngOnInit() {
    this.store.dispatch(new ForceSetPassword());
  }
}
