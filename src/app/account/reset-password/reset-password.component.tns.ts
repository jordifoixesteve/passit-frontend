import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  OnInit
} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { FormGroupState } from "ngrx-forms";

import { IResetPasswordForm } from "./reset-password.reducer";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResetPasswordComponent implements OnInit {
  @Input()
  isExtension: boolean;
  @Input()
  form: FormGroupState<IResetPasswordForm>;
  @Input()
  errorMessage: string;
  @Input()
  hasStarted: boolean;
  @Input()
  hasFinished: boolean;

  @Output()
  submitEmail = new EventEmitter();
  @Output()
  reset = new EventEmitter();

  constructor(private routerExtensions: RouterExtensions) {}

  ngOnInit() {}

  onSubmit() {
    if (this.form.isValid) {
      this.submitEmail.emit();
      //   this.emailInput.nativeElement.blur();
    }
  }

  onReset() {
    this.reset.emit();
    // setTimeout(() => this.emailInput.nativeElement.focus(), 0);
  }

  goToLogin() {
    this.routerExtensions.navigate(["/login"], { clearHistory: true });
  }
}
