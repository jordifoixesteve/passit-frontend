import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { withLatestFrom, map, mergeMap, tap, catchError } from "rxjs/operators";
import { Store, select } from "@ngrx/store";
import { IState } from "../../../app.reducers";
import { getResetPasswordVerifyEmailAndCode } from "../../account.reducer";
import { UserService } from "../../user";
import {
  VerifyAndLogin,
  ResetPasswordVerifyActionTypes,
  VerifyAndLoginSuccess,
  VerifyAndLoginFailure
} from "./reset-password-verify.actions";
import { of } from "rxjs";
import { Router } from "@angular/router";

@Injectable()
export class ResetPasswordVerifyEffects {
  @Effect()
  verifyAndLogin$ = this.actions$.pipe(
    ofType<VerifyAndLogin>(ResetPasswordVerifyActionTypes.VERIFY_AND_LOGIN),
    withLatestFrom(this.store.pipe(select(getResetPasswordVerifyEmailAndCode))),
    map(([action, emailAndCode]) => [
      action.payload,
      emailAndCode.email,
      emailAndCode.emailCode
    ]),
    mergeMap(([backupCode, email, emailCode]) => {
      if (email && emailCode) {
        return this.userService.resetPasswordVerify(email, emailCode).pipe(
          mergeMap(resp =>
            this.userService.resetPasswordVerifyLogin(
              email,
              emailCode,
              resp.private_key_backup,
              backupCode!,
              resp.server_public_key
            )
          ),
          map(auth => new VerifyAndLoginSuccess(auth)),
          catchError(err => of(new VerifyAndLoginFailure(err)))
        );
      }
      return of(new VerifyAndLoginFailure("Invalid recovery link"));
    })
  );

  @Effect({ dispatch: false })
  verifyAndLoginSuccess$ = this.actions$.pipe(
    ofType<VerifyAndLoginSuccess>(
      ResetPasswordVerifyActionTypes.VERIFY_AND_LOGIN_SUCCESS
    ),
    tap(() => {
      this.router.navigate(["reset-password/set-password"]);
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private router: Router,
    private userService: UserService
  ) {}
}
