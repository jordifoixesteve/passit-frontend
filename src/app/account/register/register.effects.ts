import {
  map,
  exhaustMap,
  filter,
  tap,
  withLatestFrom,
  catchError
} from "rxjs/operators";
import { of } from "rxjs";
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store, select } from "@ngrx/store";

import * as fromAccount from "../account.reducer";
import {
  NewsletterSubscribeAction,
  RegisterActionTypes,
  RegisterFailureAction,
  CheckEmailAction,
  SetIsEmailTaken,
  CheckEmailSuccess,
  CheckUrlAction,
  CheckUrlFailureAction,
  CheckUrlSuccessAction,
  RegisterAction,
  CheckEmailFailure,
  RegisterSuccessAction
} from "./register.actions";
import { UserService } from "../user";
import { MoonMail } from "../moonmail/moonmail.service";
import { IState } from "../../app.reducers";

@Injectable()
export class RegisterEffects {
  @Effect()
  checkEmail$ = this.actions$.pipe(
    ofType<CheckEmailAction>(RegisterActionTypes.CHECK_EMAIL),
    withLatestFrom(this.store.pipe(select(fromAccount.getRegisterForm))),
    map(([action, form]) => form),
    exhaustMap(form => {
      return this.userService.checkUsername(form.value.email).pipe(
        map(resp => {
          if (resp.isAvailable) {
            return new CheckEmailSuccess();
          } else {
            return new SetIsEmailTaken(true);
          }
        }),
        catchError(err => of(new CheckEmailFailure(err)))
      );
    })
  );

  @Effect()
  checkUrl$ = this.actions$.pipe(
    ofType<CheckUrlAction>(RegisterActionTypes.CHECK_URL),
    withLatestFrom(this.store.pipe(select(fromAccount.getUrlForm))),
    map(([action, form]) => form),
    exhaustMap(form => {
      return this.userService.checkAndSetUrl(form.value.url).pipe(
        map(() => new CheckUrlSuccessAction()),
        catchError(() => of(new CheckUrlFailureAction()))
      );
    })
  );

  @Effect({ dispatch: false })
  newsletterSubscribe$ = this.actions$.pipe(
    ofType<NewsletterSubscribeAction>(RegisterActionTypes.NEWSLETTER_SUBSCRIBE),
    withLatestFrom(this.store.pipe(select(fromAccount.getRegisterForm))),
    map(([action, form]) => form),
    filter(form => form.value.signUpNewsletter),
    map(form => form.value.email),
    tap(email => this.moonmailService.subscribeEmail(email))
  );

  @Effect()
  register$ = this.actions$.pipe(
    ofType<RegisterAction>(RegisterActionTypes.REGISTER),
    withLatestFrom(this.store.pipe(select(fromAccount.getRegisterForm))),
    map(([action, form]) => form.value),
    exhaustMap(auth =>
      this.userService
        .register(
          auth.email,
          auth.password,
          auth.rememberMe ? auth.rememberMe : false
        )
        .pipe(
          map(resp => new RegisterSuccessAction(resp)),
          catchError(err => of(new RegisterFailureAction(err)))
        )
    )
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private moonmailService: MoonMail,
    private store: Store<IState>
  ) {}
}
