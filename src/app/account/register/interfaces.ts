import { IAuthStore } from "../user";

export interface IRegisterForm {
  email: string;
  password: string;
  passwordConfirm: string;
  showConfirm: boolean;
  signUpNewsletter: boolean;
  rememberMe?: boolean;
}

export interface IUrlForm {
  url: string;
}

export interface IRegisterSuccessResponse extends IAuthStore {
  backupCode: string;
}
