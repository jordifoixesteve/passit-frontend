import { HttpClientModule } from "@angular/common/http";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { InlineSVGModule } from "ng-inline-svg";
import { NgrxFormsModule } from "ngrx-forms";
import { StoreModule, Store } from "@ngrx/store";
import * as fromAccount from "../account.reducer";
import * as fromRoot from "../../app.reducers";
import { RouterTestingModule } from "@angular/router/testing";
import { UserService } from "../user";
import { SharedModule } from "../../shared/shared.module";
import { ErrorReportingContainer } from "./error-reporting.container";
import { ErrorReportingComponent } from "./error-reporting.component";
import { EffectsModule } from "@ngrx/effects";
import { ErrorReportingEffects } from "./error-reporting.effects";
import { LoginSuccessAction } from "../account.actions";
import { ProgressIndicatorModule } from "../../progress-indicator/progress-indicator.module";

describe("Change Password Component", () => {
  let component: ErrorReportingContainer;
  let fixture: ComponentFixture<ErrorReportingContainer>;
  let service: UserService;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgrxFormsModule,
        InlineSVGModule.forRoot(),
        RouterTestingModule,
        SharedModule,
        ProgressIndicatorModule,
        HttpClientModule,
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("account", fromAccount.reducers),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([ErrorReportingEffects])
      ],
      declarations: [ErrorReportingContainer, ErrorReportingComponent],
      providers: [
        {
          provide: UserService,
          useValue: jasmine.createSpyObj("UserService", ["setErrorReporting"])
        }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(ErrorReportingContainer);
    service = TestBed.get(UserService);
    store = TestBed.get(Store);
    component = fixture.componentInstance;
  });

  it("should exist", () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(
      fixture.debugElement.nativeElement.querySelector(
        "#ErrorReportingForm\\.optInErrorReporting"
      ).checked
    ).toBeFalsy();
  });

  it("should submit response via user service", () => {
    fixture.debugElement.nativeElement.querySelector("#submit-button").click();
    expect(service.setErrorReporting).toHaveBeenCalledWith(false);
  });

  it("should default to current opt in status", () => {
    // Log in
    store.dispatch(
      new LoginSuccessAction({
        email: "",
        optInErrorReporting: true,
        privateKey: "",
        publicKey: "",
        rememberMe: false,
        userId: 1,
        userToken: ""
      })
    );
    fixture.detectChanges();
    expect(
      fixture.debugElement.nativeElement.querySelector(
        "#ErrorReportingForm\\.optInErrorReporting"
      ).checked
    ).toBeTruthy();
  });
});
