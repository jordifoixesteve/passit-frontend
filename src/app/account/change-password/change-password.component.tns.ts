import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormGroupState, MarkAsSubmittedAction } from "ngrx-forms";
import { ActionsSubject } from "@ngrx/store";

import { IChangePasswordForm } from "../change-password/change-password.reducer";

@Component({
  selector: "change-password",
  moduleId: module.id,
  templateUrl: "./change-password.component.html"
})
export class ChangePasswordComponent {
  @Input() form: FormGroupState<IChangePasswordForm>;
  @Input() hasStarted: boolean;
  @Input() hasFinished: boolean;
  @Input() nonFieldErrors: string[];
  @Output() changePassword = new EventEmitter();
  @Output() toggleConfirm = new EventEmitter();

  constructor(private actionsSubject: ActionsSubject) {}

  onSubmit() {
    if (this.form.isValid) {
      this.changePassword.emit();
    } else if (this.form.isUnsubmitted) {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
    }
  }
}
