import { Injectable } from "@angular/core";
import { Action, Store, select } from "@ngrx/store";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { Observable, of } from "rxjs";
import { withLatestFrom, map, catchError, exhaustMap } from "rxjs/operators";
import {
  ChangePasswordActionTypes,
  ChangePasswordSubmitForm,
  ChangePasswordSubmitFormSuccess,
  ChangePasswordSubmitFormFailure
} from "./change-password.actions";
import * as fromAccount from "../account.reducer";
import { UserService } from "../user";
import { NgPassitSDK } from "~/app/ngsdk/sdk";

@Injectable()
export class ChangePasswordEffects {
  constructor(
    private actions$: Actions,
    private userService: UserService,
    private sdk: NgPassitSDK,
    private store: Store<any>
  ) {}

  @Effect()
  changePassword$: Observable<Action> = this.actions$.pipe(
    ofType<ChangePasswordSubmitForm>(ChangePasswordActionTypes.SUBMIT_FORM),
    withLatestFrom(this.store.pipe(select(fromAccount.changePassword))),
    map(([action, form]) => form.value),
    exhaustMap(formValue =>
      this.userService
        .changePassword(formValue.oldPassword, formValue.newPassword)
        .pipe(
          map(resp => {
            this.sdk.set_keys(resp.publicKey, resp.privateKey);
            return new ChangePasswordSubmitFormSuccess({
              backupCode: resp.backupCode,
              token: resp.token,
              privateKey: resp.privateKey,
              publicKey: resp.publicKey
            });
          }),
          catchError(err => {
            if (err.res.status === 400) {
              return of(
                new ChangePasswordSubmitFormFailure(["Incorrect password"])
              );
            } else {
              return of(
                new ChangePasswordSubmitFormFailure(["Unexpected error."])
              );
            }
          })
        )
    )
  );
}
