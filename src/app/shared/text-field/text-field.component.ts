import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from "@angular/core";
import { AbstractControlState } from "ngrx-forms";

enum TextFieldType {
  email = "email",
  password = "password",
  url = "url"
}

@Component({
  selector: "app-text-field",
  templateUrl: "./text-field.component.html",
  styleUrls: ["./text-field.styles.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextFieldComponent {
  @Input() label: string;
  /* Optional is complete status, shows field as a checked off, no longer editable label */
  @Input() isComplete = false;
  @Input() ngrxFormControl: AbstractControlState<string>;
  @Input() type: TextFieldType;
  @Input() tabIndex: number;
  @Input() autocapitalizationType: string;
  @Input() autocorrect: boolean;
  @Input() editable = true;
  @Input() isFormSubmitted: boolean;
  @Input() hint = "";
  @Input() returnKeyType: string;
  @Input() width: number;
  @Input() extraValidations = true;
  @Input() overrideValidStyling = false;
  @Input() showErrorBeforeSubmit = false;
  @Output() returnPress = new EventEmitter();
  @Output() focus = new EventEmitter();
  @ViewChild("textFieldInput") textFieldInput: ElementRef;

  constructor() {}

  hasErrors() {
    return Object.keys(this.ngrxFormControl.errors).length !== 0;
  }

  /** Maps type to NS keyboard type */
  getKeyboardType() {
    switch (this.type) {
      case TextFieldType.email:
        return "email";
      case TextFieldType.url:
        return "url";
    }
  }

  getSecure() {
    switch (this.type) {
      case TextFieldType.password:
        return true;
      default:
        return false;
    }
  }

  focusInput = () => {
    if (this.textFieldInput) {
      this.textFieldInput.nativeElement.focus();
    }
  };
}
