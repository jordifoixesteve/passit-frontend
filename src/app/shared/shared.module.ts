import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { InlineSVGModule } from "ng-inline-svg";
import { TooltipModule } from "ngx-tooltip";

import { NavbarComponent } from "./navbar/navbar.component";
import { NavbarContainer } from "./navbar/navbar.container";
import { SearchComponent } from "./search";
import { NgrxFormsModule } from "ngrx-forms";
import { CheckboxComponent } from "./checkbox/checkbox.component";
import { NonFieldMessagesComponent } from "./non-field-messages/non-field-messages.component";
import { FormLabelComponent } from "./form-label/form-label.component";
import { AsideLinkComponent } from "./aside-link/aside-link.component";
import { TextLinkComponent } from "./text-link/text-link.component";
import { TextFieldComponent } from "./text-field/text-field.component";
import { HeadingComponent } from "./heading/heading.component";

export const COMPONENTS = [
  NavbarContainer,
  NavbarComponent,
  SearchComponent,
  CheckboxComponent,
  NonFieldMessagesComponent,
  FormLabelComponent,
  AsideLinkComponent,
  TextFieldComponent,
  HeadingComponent,
  TextLinkComponent
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    InlineSVGModule,
    TooltipModule,
    NgrxFormsModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class SharedModule {}
