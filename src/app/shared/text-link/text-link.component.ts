import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  AfterViewInit,
  Renderer2,
  ViewChild
} from "@angular/core";

@Component({
  selector: "app-text-link",
  templateUrl: "./text-link.component.html",
  styleUrls: ["./text-link.component.scss", "./text-link.component.tns.css"]
})
export class TextLinkComponent implements AfterViewInit {
  @Input() caret: "right" | "left" | null;
  @Input() isMinor: boolean;
  @Input() target = "_self";
  @Input() link: string[] | string;
  /** Only use href for external urls */
  @Input() isExternal = false;
  @Input() smartBold = true;
  @ViewChild("ref") ref: ElementRef<HTMLAnchorElement>;
  fontWeight: number | null = null;
  bolderUnderline = false;
  renderer: Renderer2;
  changeDetector: ChangeDetectorRef;

  constructor(renderer: Renderer2, cdr: ChangeDetectorRef) {
    this.renderer = renderer;
    this.changeDetector = cdr;
  }

  ngAfterViewInit() {
    this.updateBoldAndUnderline();
    this.trimSpaces();
    this.changeDetector.detectChanges();
  }

  updateBoldAndUnderline = () => {
    const computedStyle = window.getComputedStyle(this.ref.nativeElement);
    if (computedStyle) {
      const computedSize = computedStyle.fontSize;
      const computedWeight = computedStyle.fontWeight;
      const fontWeight = computedWeight ? parseInt(computedWeight, 10) : -1;
      const fontSize = computedSize ? parseInt(computedSize, 10) : -1;
      if (this.smartBold && fontSize > 0 && fontSize < 20) {
        if (fontWeight === 400) {
          this.fontWeight = 600;
        }

        if (fontWeight === 600) {
          this.fontWeight = 700;
        }
      }
      if (fontSize > 16) {
        this.bolderUnderline = true;
      }
    }
  }

  trimSpaces = () => {
    if (this.ref && this.ref.nativeElement.childNodes.length) {
      const childNodes = this.ref.nativeElement.childNodes;
      // Filter out nodes that are comments, using
      // https://stackoverflow.com/a/49822981/ to cast a NodeList
      // to an array
      const filteredNodes: Node[] = Array.prototype.slice.call(childNodes).filter((node: Node) => node.nodeType !== 8);
      const firstChildNode = filteredNodes[0];
      // NodeList. If only 1 child, and that child is a text node
      // (numeric constants 🙄) then we want to trim
      // https://stackoverflow.com/a/36545542/ for renderer help
      if (
        filteredNodes.length === 1 &&
        firstChildNode.nodeType === 3 &&
        firstChildNode.nodeValue
      ) {
        this.renderer.setValue(firstChildNode, firstChildNode.nodeValue.trim());
      } else if (firstChildNode.nodeType === 1) {
        // if nodeType is 1 we can cast to an Element type
        const firstChildDisplayStyle = window.getComputedStyle(
          firstChildNode as Element
        ).display;
        if (firstChildDisplayStyle === "block") {
          throw new Error(
            "You used <app-text-link> and put block-level content inside. Don't do that!"
          );
        }
      }
    }
  }
}
