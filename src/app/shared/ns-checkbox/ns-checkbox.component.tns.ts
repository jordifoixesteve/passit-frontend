import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from "@angular/core";

/**
 * A declarative wrapper around CheckBox
 * Makes it compatible with tap and ensures the native CheckBox does not get out of
 * sync with it's bound checked value.
 */
@Component({
  selector: "ns-checkbox",
  template: `
    <CheckBox
      #CB
      [checked]="checked"
      (tap)="onTap()"
      (checkedChange)="checkedChange($event.value)"
    ></CheckBox>
  `
})
export class NsCheckboxComponent {
  @Input() checked = false;
  @Output() tap = new EventEmitter();
  @ViewChild("CB") checkbox: ElementRef;

  onTap() {}

  checkedChange(isChecked: boolean) {
    if (isChecked !== this.checked) {
      this.checkbox.nativeElement.checked = this.checked;
    }
  }
}
