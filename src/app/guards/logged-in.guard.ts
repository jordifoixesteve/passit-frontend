import { map, take } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Store, select } from "@ngrx/store";

import { LoginRedirect } from "../account/account.actions";
import * as fromAccount from "../account/account.reducer";

@Injectable()
export class LoggedInGuard implements CanActivate {
  constructor(
    public store: Store<fromAccount.IAuthState>,
    public router: Router
  ) {}

  public canActivate() {
    return this.store.pipe(select(fromAccount.selectAuthState)).pipe(
      take(1),
      map(authState => {
        if (!authState.userToken) {
          this.store.dispatch(new LoginRedirect());
          return false;
        } else if (authState.forceSetPassword) {
          this.router.navigate(["/reset-password/set-password"]);
          return false;
        }

        return true;
      })
    );
  }
}
