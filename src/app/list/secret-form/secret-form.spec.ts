import { SecretFormContainer } from "./secret-form.container";
import {
  ComponentFixture,
  async,
  TestBed,
  fakeAsync,
  tick
} from "@angular/core/testing";
import { NgrxFormsModule, SetValueAction, box } from "ngrx-forms";
import { InlineSVGModule } from "ng-inline-svg";
import { StoreModule, Store } from "@ngrx/store";

import * as fromList from "../list.reducer";
import * as fromAccount from "../../account/account.reducer";
import * as fromRoot from "../../app.reducers";
import { SecretFormComponent } from "./secret-form.component";
import { ClipboardModule } from "ngx-clipboard";
import { SelectModule } from "ng-select";
import { groupReducer } from "../../group/group.reducer";
import { GeneratorService } from "../../secrets";
import { EffectsModule } from "@ngrx/effects";
import { SecretFormEffects } from "./secret-form.effects";
import { SecretService } from "../../secrets/secret.service";
import { SetGroupsAction } from "../../group/group.actions";

class FakeService {}

describe("Secret Form", () => {
  let component: SecretFormContainer;
  let fixture: ComponentFixture<SecretFormContainer>;
  let generatorService: any;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgrxFormsModule,
        InlineSVGModule.forRoot(),
        ClipboardModule,
        SelectModule,
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("account", fromAccount.reducers),
        StoreModule.forFeature("list", fromList.reducers),
        StoreModule.forFeature("group", groupReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([SecretFormEffects])
      ],
      declarations: [SecretFormContainer, SecretFormComponent],
      providers: [
        {
          provide: GeneratorService,
          useValue: jasmine.createSpyObj("generatorService", [
            "generatePassword"
          ])
        },
        { provide: SecretService, useClass: FakeService }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecretFormContainer);
    component = fixture.componentInstance;
    generatorService = TestBed.get(GeneratorService);
    store = TestBed.get(Store);
  });

  it("should exist", () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it("should require a name", () => {
    fixture.detectChanges();
    fixture.debugElement.nativeElement.querySelector("#submit-button").click();
    fixture.detectChanges();
    expect(fixture.nativeElement.innerText.indexOf("Required") !== -1).toBe(
      true
    );
  });

  it(
    "should set a password",
    fakeAsync(() => {
      const fakeRandomPassword = "fj3092fn302wign3q4it";
      generatorService.generatePassword.and.returnValue(
        Promise.resolve(fakeRandomPassword)
      );
      fixture.detectChanges();
      fixture.debugElement.nativeElement
        .querySelector("#generatePasswordButton")
        .click();
      tick(100);
      fixture.detectChanges();
      const passwordInput = fixture.debugElement.nativeElement.querySelector(
        "#Secret\\ Form\\.password"
      );
      expect(passwordInput.value).toBe(fakeRandomPassword);
    })
  );

  it("should show selected groups", () => {
    store.dispatch(
      new SetGroupsAction([
        {
          id: 1,
          name: "My Group",
          groupuser_set: [],
          my_key_ciphertext: "",
          my_private_key_ciphertext: "",
          public_key: ""
        }
      ])
    );
    store.dispatch(new SetValueAction("Secret Form.groups", box([1])));
    fixture.detectChanges();
    const groupsInput = fixture.debugElement.nativeElement.querySelector(
      "#Secret\\ Form\\.groups"
    );
    expect(groupsInput.innerText).toContain("My Group");
  });
});
