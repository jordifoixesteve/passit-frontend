import {
  Component,
  ChangeDetectorRef,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from "@angular/core";

import * as Clipboard from "nativescript-clipboard";

import * as api from "../../../passit_sdk/api.interfaces";

import { FormGroupState } from "ngrx-forms";
import { ISecretForm } from "./secret-form.reducer";
import { IS_TABLET } from "./constants.tns";
import * as utils from "tns-core-modules/utils/utils";

interface ISelectOptions {
  label: string;
  value: any;
}

@Component({
  selector: "secret-form-component",
  moduleId: module.id,
  templateUrl: "./secret-form.component.html"
})
export class SecretFormComponent {
  @Input() form: FormGroupState<ISecretForm>;
  @Input() isNew: boolean;
  @Input() isUpdating: boolean;
  @Input() isUpdated: boolean;
  @Input() passwordIsMasked: boolean;
  @Input() showNotes: boolean;
  @Input() usernameCopied: boolean;
  @Input() passwordCopied: boolean;

  @Output() togglePasswordIsMasked = new EventEmitter();
  @Output() setPasswordIsMasked = new EventEmitter<boolean>();
  @Output() toggleShowNotes = new EventEmitter();
  @Output() hideAddSecretForm = new EventEmitter();
  @Output() saveSecret = new EventEmitter<boolean>();
  @Output() deleteSecret = new EventEmitter();
  @Output() generatePassword = new EventEmitter();
  @Output() cancel = new EventEmitter();

  isTablet = IS_TABLET;
  groupOptions: ISelectOptions[] = [];

  @Input()
  set groups(groups: api.IGroup[]) {
    this.groupOptions = groups.map(group => {
      return {
        label: group.name,
        value: group.id
      };
    });
  }

  @ViewChild("realPasswordInput") realPasswordInput: ElementRef;

  constructor(private cd: ChangeDetectorRef) {}

  onSubmit() {
    if (this.form.isValid) {
      this.saveSecret.emit(this.isNew);
    }
  }

  clickMaskedSecret() {
    this.togglePasswordIsMasked.emit();
    setTimeout(() => this.realPasswordInput.nativeElement.focus(), 0);
  }

  toggleViewSecret() {
    this.togglePasswordIsMasked.emit();
    setTimeout(() => this.realPasswordInput.nativeElement.focus(), 0);
  }

  togglePassword() {
    this.togglePasswordIsMasked.emit();
  }

  onDeleteSecret() {
    const confirmOptions: any = {
      title: "Delete " + this.form.value.name + "?",
      message: "Once it's deleted, it's gone forever. Is that ok?",
      okButtonText: "Delete",
      cancelButtonText: "Cancel"
    };
    (confirm(confirmOptions) as any).then((result: any) => {
      if (result) {
        this.deleteSecret.emit();
      }
    });
  }

  copyUsername() {
    Clipboard.setText(this.form.value.username).then(() =>
      this.triggerCopied("username")
    );
  }

  copyPassword() {
    Clipboard.setText(this.form.value.password).then(() =>
      this.triggerCopied("password")
    );
  }

  realPasswordBlur() {
    this.setPasswordIsMasked.emit(true);
  }

  goToUrl() {
    let url = this.form.value.url;
    if (!url.match(/^https?:\/\//)) {
      url = "http://" + url;
    }
    utils.openUrl(url);
  }

  triggerCopied(field: string) {
    if (field === "username") {
      this.usernameCopied = true;
      setTimeout(() => {
        this.usernameCopied = false;
        this.cd.markForCheck();
      }, 2000);
    }
    if (field === "password") {
      this.passwordCopied = true;
      setTimeout(() => {
        this.passwordCopied = false;
        this.cd.markForCheck();
      }, 2000);
    }
  }
}
