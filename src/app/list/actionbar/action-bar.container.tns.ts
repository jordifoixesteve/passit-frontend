import { Component } from "@angular/core";
import { Store } from "@ngrx/store";

import * as fromRoot from "../../app.reducers";
import { ToggleMenu } from "../../mobile-menu/mobile-menu.actions";
import { SetSearchText } from "../../list/list.actions";

@Component({
  selector: "ns-list-action-bar-container",
  moduleId: module.id,
  template: `
    <ns-list-action-bar-component
      [showSearch]="showSearch"
      (menuTap)="menuTap()"
      (searchTap)="searchTap()"
      (search)="search($event)"
      (clearSearch)="clearSearch()"
    ></ns-list-action-bar-component>
  `
})
export class ListActionBarContainer {
  showSearch = false;

  constructor(private store: Store<fromRoot.IState>) {}

  menuTap() {
    this.store.dispatch(new ToggleMenu());
  }

  searchTap() {
    this.showSearch = !this.showSearch;
  }

  search(term: string) {
    this.store.dispatch(new SetSearchText(term));
  }

  clearSearch() {
    this.showSearch = false;
    this.store.dispatch(new SetSearchText(""));
  }
}
