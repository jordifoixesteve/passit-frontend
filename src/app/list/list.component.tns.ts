import {
  Component,
  Input,
  Output,
  EventEmitter,
  AfterViewInit
} from "@angular/core";
import { DeviceType } from "ui/enums";
import { device } from "platform";

import * as api from "../../passit_sdk/api.interfaces";
import { Router } from "@angular/router";
import { SideDrawer } from "../mobile-menu/sidedrawer";

@Component({
  selector: "secret-list-component",
  moduleId: module.id,
  templateUrl: "./list.component.html"
})
export class SecretListComponent implements AfterViewInit {
  @Input() secrets: api.ISecret[];
  @Input() secretManagedObject: api.ISecret[];
  @Input() searchText: string;
  @Input() firstTimeLoadingComplete: boolean;
  @Output() secretWasSelected = new EventEmitter<number>();
  @Output() showAddSecretForm = new EventEmitter();
  isTablet: boolean = device.deviceType === DeviceType.Tablet;

  constructor(private router: Router, private sidedrawer: SideDrawer) {}

  ngAfterViewInit() {
    setTimeout(() => this.sidedrawer.build(this));
  }

  onSecretWasSelected(secret: api.ISecret) {
    this.secretWasSelected.emit(secret.id);
    if (!this.isTablet) {
      // On phone - send to a new page
      this.router.navigate(["/list", secret.id]);
    }
  }

  addNewSecret() {
    this.showAddSecretForm.emit();
    this.router.navigate(["/list/new"]);
  }
}
