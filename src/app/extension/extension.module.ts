import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { InlineSVGModule } from "ng-inline-svg";
import { TooltipModule } from "ngx-tooltip";
import { HotkeyModule } from "angular2-hotkeys";

import { PopupComponent } from "./popup/popup.component";
import { PopupItemComponent } from "./popup/popup-item/popup-item.component";
import { PopupOnboardingComponent } from "./popup/popup-onboarding/popup-onboarding.component";
import { PopupContainer } from "./popup/popup.container";
import { popupReducer } from "./popup/popup.reducer";
import { SharedModule } from "../shared/shared.module";
import { PopupEffects } from "./popup/popup.effects";

export const COMPONENTS = [
  PopupComponent,
  PopupContainer,
  PopupItemComponent,
  PopupOnboardingComponent
];

@NgModule({
  imports: [
    CommonModule,
    HotkeyModule,
    InlineSVGModule,
    SharedModule,
    TooltipModule,
    ScrollingModule,
    StoreModule.forFeature("popup", popupReducer),
    EffectsModule.forFeature([PopupEffects])
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ExtensionModule {}
