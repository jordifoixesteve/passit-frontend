/*
 * @angular
 */
import { APP_BASE_HREF } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule, ErrorHandler, ApplicationRef } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Params } from "@angular/router";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { routingStore, AppRoutingModule } from "./app-routing.module";

/*
 * Third Party Other
 */
import { EffectsModule } from "@ngrx/effects";
import { RouterStateSerializer } from "@ngrx/router-store";
import { StoreModule, Store } from "@ngrx/store";
import { InlineSVGModule } from "ng-inline-svg";
import { SelectModule } from "ng-select";
import { TooltipModule } from "ngx-tooltip";
import { HotkeyModule } from "angular2-hotkeys";

/*
 * Passit
 */
import { AccountModule } from "./account/account.module";
import { MoonMail } from "./account/moonmail/moonmail.service";
import { AppComponent } from "./app.component";
import { ResetFormModule } from "./form/reset-form.module";
import { GroupModule } from "./group/group.module";
import { AlreadyLoggedInGuard, LoggedInGuard } from "./guards";
import { ListModule } from "./list";
import { NoContentComponent } from "./no-content/no-content.component";
import { NoContentContainer } from "./no-content/no-content.container";
import { SharedModule } from "./shared/shared.module";

import { ExtensionModule } from "./extension/extension.module";
import { ProgressIndicatorModule } from "./progress-indicator/progress-indicator.module";

import { metaReducers, reducers } from "./app.reducers";
import { IS_EXTENSION } from "./constants";
import { GetConfService } from "./get-conf/";
import { GetConfEffects } from "./get-conf/conf.effects";
import { Api } from "./ngsdk/api";
import { NgPassitSDK } from "./ngsdk/sdk";
import { GeneratorService } from "./secrets";
import { SecretEffects } from "./secrets/secret.effects";
import { SecretService } from "./secrets/secret.service";
import { AppDataService } from "./shared/app-data/app-data.service";
import { PopupLoggedInGuard } from "./guards/popup-logged-in.guard";
import { RavenErrorHandler } from "./error-handler";
import { AuthInterceptor } from "./api/auth.interceptor";
import { devtoolsModule } from "./store-devtools";
import { take } from "rxjs/operators";
import {
  createNewHosts,
  createInputTransfer,
  removeNgStyles
} from "@angularclass/hmr";

// Why is this not default ngrx store, why is crashing default?
export interface IRouterStateUrl {
  url: string;
  queryParams: Params;
}

export class CustomSerializer
  implements RouterStateSerializer<IRouterStateUrl> {
  serialize(routerState: any): IRouterStateUrl {
    const { url } = routerState;
    const queryParams = routerState.root.queryParams;

    // Only return an object including the URL and query params
    // instead of the entire snapshot
    return { url, queryParams };
  }
}

/* tslint:disable:object-literal-sort-keys */
@NgModule({
  declarations: [AppComponent, NoContentContainer, NoContentComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HotkeyModule.forRoot(),
    BrowserAnimationsModule,
    AccountModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([SecretEffects, GetConfEffects]),
    devtoolsModule,
    FormsModule,
    GroupModule,
    ListModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    ProgressIndicatorModule,
    ReactiveFormsModule,
    ResetFormModule,
    routingStore,
    SelectModule,
    !IS_EXTENSION
      ? ServiceWorkerModule.register("ngsw-worker.js", {
          enabled: environment.production
        })
      : [],
    SharedModule,
    TooltipModule,
    ExtensionModule
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: "/"
    },
    GetConfService,
    { provide: AppDataService, useClass: AppDataService },
    AlreadyLoggedInGuard,
    LoggedInGuard,
    PopupLoggedInGuard,
    { provide: MoonMail, useClass: MoonMail },
    { provide: SecretService, useClass: SecretService },
    GeneratorService,
    { provide: RouterStateSerializer, useClass: CustomSerializer },
    { provide: Api, useClass: Api },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: NgPassitSDK, useClass: NgPassitSDK },
    { provide: ErrorHandler, useClass: RavenErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private appRef: ApplicationRef, private store: Store<any>) {}

  public hmrOnInit(store: any) {
    if (!store || !store.state) {
      return;
    }
    // restore state
    this.store.dispatch({ type: "SET_ROOT_STATE", payload: store.state });
    // restore input values
    if ("restoreInputValues" in store) {
      const restoreInputValues = store.restoreInputValues;
      // this isn't clean but gets the job done in development
      setTimeout(restoreInputValues);
    }
    this.appRef.tick();
    Object.keys(store).forEach(prop => delete store[prop]);
  }

  public hmrOnDestroy(store: any) {
    const cmpLocation = this.appRef.components.map(
      cmp => cmp.location.nativeElement
    );
    let currentState: any;
    this.store.pipe(take(1)).subscribe(state => (currentState = state));
    store.state = currentState;
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues = createInputTransfer();
    removeNgStyles();
  }

  public hmrAfterDestroy(store: any) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
