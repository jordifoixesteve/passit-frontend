// tslint:disable:max-line-length
import { IGroupForm } from "./group.interfaces";
import { GroupService } from "./group.service";
import { IContact } from "./contacts/contacts.interfaces";

const MyUserID = 1;
// const OtherUserID = 2;

// const groupResp = {
//   id: 1,
//   name: "test group",
//   groupuser_set: [
//     {id: 1, user: MyUserID, group: 1, is_group_admin: true}
//   ],
//   public_key: "dR+fvhZ5NToyPjzjGNkLxsshMAlLoSH+OEh58mFUNmM=",
//   my_key_ciphertext: "SVpSTrj+nXBsPW0Zg3S8umBJ9nvUTxZVK4hcIAx/CE+vkSeNnmJEAvtuvhs2bCj4rVdLEsZLb9KB1ue7vjrdF0PgXe/OUBUAbnZLBElm17kUbjUzLe7fWAgdyLw=",
//   my_private_key_ciphertext: "CEZNxwMS6+4a7xtAv5e6YtMfWyatFbASUCvru5DCp6YZtQqCoFo6Fe9yNbBcbjsb+x/b5NiM9eXRihsXj1oIR00Xe9skRQuxGyIoJCFgKMA+OcHM",
// };

class FakeSDK {
  userId = MyUserID;

  create_group(name: string, slug?: string) {
    return new Promise((resolve, reject) => {
      resolve({
        id: 1,
        name,
        slug
      });
    });
  }

  update_group(group: IGroupForm) {
    return new Promise((resolve, reject) => {
      resolve({
        id: group.id,
        group: group.name,
        slug: group.slug
      });
    });
  }

  get_group(groupID: number) {
    return new Promise((resolve, reject) => {
      resolve({
        id: 1,
        name: "devs",
        groupuser_set: [
          { id: 1, user: MyUserID, group: 1, is_group_admin: true }
        ],
        public_key: "7A1ijT2S83er1A/vDHGd0pEloQtcRiMyZ9+P1MPCyWI=",
        my_key_ciphertext:
          "e060zKeVF5CvSeR0tmNspnK/l/Yp6WiiYuG+g3u0SGcI93tS/v987EtoeI/to2dCINjIFXzHM0cFyaMrxDAw8d4ftx8UJcvQ3ygYc/NS0m0KzwB7bLv664irPmM=",
        my_private_key_ciphertext:
          "WC3b++WgZLwZlOI/jDLmF4GnPzKSzZm7Sm7rJIgpFjPbmBqvM2OVuO2maVzbpgBTbrkTZRokPACOGfId9JvpEetEfn92JjEr0dIhqXIEHqz3ciY/"
      });
    });
  }
}

describe("Group Service", () => {
  let service: GroupService;
  let sdk: any;

  it("create new secret and save in store", (done: any) => {
    // Should test that the current user is ALWAYS added to a new group
    sdk = new FakeSDK();
    service = new GroupService(sdk);
    const form: IGroupForm = {
      name: "Test Group",
      slug: "test-group",
      members: [1]
    };
    const members: IContact[] = [
      {
        id: 1,
        email: "test@example.com",
        first_name: "",
        last_name: ""
      }
    ];
    service.create(form, members).subscribe(updatedGroup => {
      /* There isn't anything to test. SDK is a mocked black box.
      I could test get_group, but that would be mocked too! So this really only tests
      that things don't error. */
      done();
    });
  });
});
