import { DebugElement } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { FormBuilder, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { InlineSVGModule } from "ng-inline-svg";
import { SelectModule } from "ng-select";
import { ClipboardModule } from "ngx-clipboard";
import { newEvent } from "../../testing";
import { GroupFormComponent } from "./group-form.component";

let fixture: ComponentFixture<GroupFormComponent>;
let component: GroupFormComponent;

class Page {
  submitSpy: jasmine.Spy;
  form: DebugElement;
  nameField: DebugElement;
  membersField: DebugElement;

  constructor() {
    this.submitSpy = spyOn(component.save, "emit");
  }

  addPageElements() {
    this.form = fixture.debugElement.query(By.css("form"));
    this.nameField = fixture.debugElement.query(By.css("#nameInput"));
    this.membersField = fixture.debugElement.query(By.css("#membersInput"));
  }

  submitForm() {
    this.form.triggerEventHandler("submit", null);
  }

  getSubmitSpyCalls() {
    return this.submitSpy.calls.any();
  }
}

let page: Page;

const testGroupForm = {
  name: "Test",
  members: []
};

describe("GroupFormComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupFormComponent],
      providers: [FormBuilder],
      imports: [
        ClipboardModule,
        ReactiveFormsModule,
        FormsModule,
        InlineSVGModule.forRoot(),
        SelectModule
      ]
    });
    createComponent();
  }));

  it("creates group form with members input", () => {
    const groupComponent: any = fixture.debugElement.query(By.css(".secret"))
      .nativeElement;
    expect(groupComponent.textContent).toContain("Members");
  });

  it("creates group form with name input", () => {
    const groupComponent: any = fixture.debugElement.query(By.css(".secret"))
      .nativeElement;
    expect(groupComponent.textContent).toContain("Name");
  });

  it("does not submit an invalid group form", () => {
    page.addPageElements();

    component.groupForm = {
      name: "",
      members: []
    };
    component.singleGroupPendingContacts = [];

    page.nameField.nativeElement.dispatchEvent(newEvent("input"));

    fixture.detectChanges();
    page.submitForm();
    fixture.detectChanges();

    expect(page.getSubmitSpyCalls()).toBe(false);
  });

  it("submits a valid group form", () => {
    page.addPageElements();

    component.groupForm = testGroupForm;
    component.singleGroupPendingContacts = [];
    page.nameField.nativeElement.dispatchEvent(newEvent("input"));

    fixture.detectChanges();
    page.submitForm();
    fixture.detectChanges();

    expect(page.getSubmitSpyCalls()).toBe(true);
  });
});

function createComponent() {
  fixture = TestBed.createComponent(GroupFormComponent);
  component = fixture.componentInstance;
  page = new Page();
}
