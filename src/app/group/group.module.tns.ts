import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { ContactsService } from "./contacts/contacts.service";
import { GroupService } from "./group.service";
import { GroupEffects } from "./group.effects";
import { groupReducer } from "./group.reducer";

@NgModule({
  declarations: [],
  imports: [
    NativeScriptCommonModule,
    StoreModule.forFeature("group", groupReducer),
    EffectsModule.forFeature([GroupEffects])
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [ContactsService, GroupService]
})
export class GroupModule {}
