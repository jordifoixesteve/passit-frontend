import { Component, OnInit, Input } from "@angular/core";

import { SideDrawer } from "./sidedrawer";
import { Store } from "@ngrx/store";

import * as fromRoot from "~/app/app.reducers";
import { ToggleMenu } from "./mobile-menu.actions";

@Component({
  selector: "ns-action-bar-container",
  template: `
    <ns-action-bar-component
      [title]="title"
      (menuTap)="menuTap()"
    ></ns-action-bar-component>
  `
})
export class ActionBarContainer implements OnInit {
  @Input() title: string;

  constructor(
    private sidedrawer: SideDrawer,
    private store: Store<fromRoot.IState>
  ) {}

  ngOnInit() {
    // Pass the component's context to the service
    // Delay 500ms because it crashes sometimes on back button press otherwise
    setTimeout(() => this.sidedrawer.build(this), 500);
  }

  menuTap() {
    this.store.dispatch(new ToggleMenu());
  }
}
