import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { TnsSideDrawer } from "nativescript-foss-sidedrawer";
import { NgZone } from "@angular/core";

import * as reducers from "./mobile-menu.reducers";
import { LogoutAction } from "~/app/account/account.actions";
import * as colorModule from "tns-core-modules/color";
import * as imageSource from "tns-core-modules/image-source";
import { RouterExtensions } from "nativescript-angular/router";

const Color = colorModule.Color;
const logo = imageSource.fromResource("icon_with_background");

/**
 * Declarative Wrapper around TnsSideDrawer
 * A service works well because it should be a singleton and there is no template.
 * Every action to the drawer can be done via state changes
 */
@Injectable({
  providedIn: "root"
})
export class SideDrawer {
  private TnsSideDrawer = TnsSideDrawer;
  private isBuilt = false;

  constructor(
    private store: Store<reducers.IMobileMenuState>,
    private router: RouterExtensions,
    private zone: NgZone
  ) {}

  /**
   * Run this exactly once.
   * @param context - The 'this' from a NS Component. Not sure what this does.
   */
  build(context: any) {
    // Building it multiple times causes a bug where picking a file in import
    // results in a white screen. We don't see to have a reliable way to tell if it's
    // already built. So we track it here.
    if (this.isBuilt) {
      return;
    } else {
      this.isBuilt = true;
    }
    this.TnsSideDrawer.build({
      templates: [
        { title: "Passwords" },
        // {title: "Groups"},
        { title: "Change Account Password" },
        { title: "Import" },
        { title: "Export" },
        { title: "Error Reporting" },
        { title: "Log Out" },
        { title: "Forgot Password?" }
      ],
      textColor: new Color("black"),
      headerBackgroundColor: new Color("#0092A8"),
      backgroundColor: new Color("white"),
      logoImage: logo,
      title: "Passit",
      listener: index => {
        switch (index) {
          case 0:
            this.zone.run(() =>
              this.router.navigate(["/list"], { clearHistory: true })
            );
            return;
          // case 1:
          //   this.zone.run(() =>
          //     this.router.navigate(["/groups"]));
          //   return
          case 1:
            this.zone.run(() =>
              this.router.navigate(["/account/change-password"])
            );
            return;
          case 2:
            this.zone.run(() => this.router.navigate(["/import"]));
            return;
          case 3:
            this.zone.run(() => this.router.navigate(["/export"]));
            return;
          case 4:
            this.zone.run(() =>
              this.router.navigate(["/account/error-reporting"])
            );
            return;
          case 5:
            this.store.dispatch(new LogoutAction());
            return;
          case 6:
            this.zone.run(() =>
              this.router.navigate(["/account/forgot-password"])
            );
            return;
        }
      }
    });
  }

  toggle() {
    this.TnsSideDrawer.toggle(true);
  }
}
