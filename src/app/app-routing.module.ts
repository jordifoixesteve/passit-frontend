import { ModuleWithProviders, NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { StoreRouterConnectingModule } from "@ngrx/router-store";

import { AccountComponent } from "./account/account.component";
import { ChangePasswordContainer } from "./account/change-password";
import { ConfirmEmailContainer } from "./account/confirm-email";
import { DeleteContainer } from "./account/delete/delete.container";
import { LoginContainer } from "./account/login/login.container";
import { RegisterContainer } from "./account/register/register.container";
import { IS_EXTENSION } from "./constants";
import { PopupContainer } from "./extension/popup/popup.container";
import { GroupContainer } from "./group/group.container";
import { AlreadyLoggedInGuard, LoggedInGuard } from "./guards";
import { SecretListContainer } from "./list";
import { NoContentContainer } from "./no-content/no-content.container";
import { PopupLoggedInGuard } from "./guards/popup-logged-in.guard";
import { ConfirmEmailGuard } from "./account/confirm-email/confirm-email.guard";
import { ErrorReportingContainer } from "./account/error-reporting/error-reporting.container";
import { ManageBackupCodeContainer } from "./account/manage-backup-code/manage-backup-code.container";
import { ResetPasswordContainer } from "./account/reset-password/reset-password.container";
import { ResetPasswordVerifyContainer } from "./account/reset-password/reset-password-verify/reset-password-verify.container";
import { SetPasswordContainer } from "./account/reset-password/set-password/set-password.container";
import { ForgotLearnMoreContainer } from "./account/change-password/forgot-learn-more/forgot-learn-more.container";

/* tslint:disable:object-literal-sort-keys */
const appRoutes: Routes = [
  {
    path: "login",
    component: LoginContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Log In",
      showNavBar: false
    }
  },
  { path: "", redirectTo: "list", pathMatch: "full" },
  {
    path: "account",
    component: AccountComponent,
    data: {
      title: "Account Management"
    }
  },
  {
    path: "account/change-password",
    component: ChangePasswordContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Change Account Password"
    }
  },
  {
    path: "account/forgot-password",
    component: ForgotLearnMoreContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Forgot Your Password?"
    }
  },
  {
    path: "account/change-backup-code",
    component: ManageBackupCodeContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Change Backup Code"
    }
  },
  {
    path: "account/error-reporting",
    component: ErrorReportingContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Error Reporting"
    }
  },
  {
    path: "account/delete",
    component: DeleteContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Delete Account"
    }
  },
  {
    path: "register",
    component: RegisterContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Register",
      showNavBar: false
    }
  },
  {
    path: "confirm-email",
    component: ConfirmEmailContainer,
    canActivate: [ConfirmEmailGuard],
    data: {
      title: "Confirm Email"
    }
  },
  {
    path: "confirm-email/:code",
    component: ConfirmEmailContainer,
    data: {
      title: "Confirm Email"
    }
  },
  {
    path: "reset-password",
    component: ResetPasswordContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Reset Password",
      showNavBar: false
    }
  },
  {
    path: "reset-password-verify",
    component: ResetPasswordVerifyContainer,
    data: {
      title: "Reset Password",
      showNavBar: false
    }
  },
  {
    path: "reset-password/set-password",
    component: SetPasswordContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Set Account Password",
      showNavBar: false
    }
  },
  {
    path: "list",
    component: SecretListContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Password List"
    }
  },
  {
    path: "groups",
    component: GroupContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Groups List"
    }
  },
  {
    path: "import",
    loadChildren: "./importer/importer.module#ImporterModule"
  },
  {
    path: "export",
    loadChildren: "./exporter/exporter.module#ExporterModule"
  },
  {
    path: "popup",
    component: PopupContainer,
    canActivate: [PopupLoggedInGuard],
    data: {
      showNavBar: false
    }
  },
  {
    path: "**",
    component: NoContentContainer
  }
];

const routing: ModuleWithProviders = RouterModule.forRoot(
  appRoutes,
  // use hashing when it is an extension, because the browsers can't
  // redirect other urls for SPA to the index.html
  { useHash: IS_EXTENSION }
);
export const routingStore = StoreRouterConnectingModule;

@NgModule({
  imports: [routing],
  exports: [RouterModule]
})
export class AppRoutingModule {}
