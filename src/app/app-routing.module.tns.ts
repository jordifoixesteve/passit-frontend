import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { LoginContainer } from "./account/login/login.container";
import { RegisterContainer } from "./account/register/register.container";
import { SecretListContainer } from "./list/list.container";
import { SecretDetailComponent } from "./list/detail.component";
import { SecretNewComponent } from "./list/new.component.tns";
import { AlreadyLoggedInGuard, LoggedInGuard } from "./guards";
import { ConfirmEmailContainer } from "./account/confirm-email";
import { ConfirmEmailGuard } from "./account/confirm-email/confirm-email.guard";
import { ErrorReportingContainer } from "./account/error-reporting/error-reporting.container";
import { ChangePasswordContainer } from "./account/change-password";
import { ResetPasswordContainer } from "./account/reset-password/reset-password.container";
import { ForgotLearnMoreContainer } from "./account/change-password/forgot-learn-more/forgot-learn-more.container";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "/list",
    pathMatch: "full"
  },
  {
    path: "login",
    component: LoginContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Log In",
      showNavBar: false
    }
  },
  {
    path: "register",
    component: RegisterContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Register",
      showNavBar: false
    }
  },
  {
    path: "reset-password",
    component: ResetPasswordContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Reset Password",
      showNavBar: false
    }
  },
  {
    path: "account/error-reporting",
    component: ErrorReportingContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Error Reporting"
    }
  },
  {
    path: "account/forgot-password",
    component: ForgotLearnMoreContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Forgot Your Password?"
    }
  },
  {
    path: "account/change-password",
    component: ChangePasswordContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Change Account Password"
    }
  },
  {
    path: "confirm-email",
    component: ConfirmEmailContainer,
    canActivate: [ConfirmEmailGuard],
    data: {
      title: "Confirm Email"
    }
  },
  {
    path: "list",
    component: SecretListContainer,
    canActivate: [LoggedInGuard]
  },
  {
    path: "list/:id",
    component: SecretDetailComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: "list/new",
    component: SecretNewComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: "import",
    loadChildren: "./importer/importer.module#ImporterModule"
  },
  {
    path: "export",
    loadChildren: "./exporter/exporter.module#ExporterModule"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
