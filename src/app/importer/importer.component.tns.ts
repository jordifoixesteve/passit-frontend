import { Component, EventEmitter, Input, Output, NgZone } from "@angular/core";
import { ImportableSecret } from "./importableSecret";
import { ImporterService } from "./importer.service";
import {
  Mediafilepicker,
  FilePickerOptions
} from "nativescript-mediafilepicker";
import { File } from "tns-core-modules/file-system";
import { EventData } from "tns-core-modules/ui/page/page";

@Component({
  selector: "importer-component",
  templateUrl: "./importer.component.html",
  styleUrls: ["./importer.styles.tns.scss"]
})
export class ImporterComponent {
  previewHeader = ["name", "username", "url", "password", "notes"];
  importText = "";
  autoPreview = true;
  toggleAllState = true;
  toggleAllUseMinus = false;
  tooltipDisabled = false;
  options: FilePickerOptions;
  mediafilepicker: Mediafilepicker;
  @Input() fileName: string;
  @Input() finishedSavingMessage: string;
  @Input() hasStartedSaving: boolean;
  @Input() hasFinishedSaving: boolean;
  @Input() secrets: ImportableSecret[];
  @Output() resetForm = new EventEmitter();
  @Output() resetFinishedMessage = new EventEmitter();
  @Output() setFileName = new EventEmitter<string>();
  @Output() setImportableSecrets = new EventEmitter<ImportableSecret[]>();
  @Output() saveSecrets = new EventEmitter<ImportableSecret[]>();

  constructor(private importerService: ImporterService, private zone: NgZone) {}

  onSubmit() {
    this.saveSecrets.emit(this.secrets);
  }

  onChecked(value: boolean) {
    // NS can't do binding and start as true
    if (value && this.autoPreview === false) {
      this.autoPreview = true;
    } else if (!value && this.autoPreview === true) {
      this.autoPreview = false;
    }
  }

  templateSelector(item: any, index: number) {
    if (index === 0) {
      return "header";
    }
    return "cell";
  }

  /** File picker does not seem to work in emulator */
  selectCSV() {
    const extensions = ["csv"];
    const options = {
      android: {
        extensions: extensions,
        maxNumberFiles: 1
      },
      ios: {
        extensions: extensions,
        multipleSelection: true
      }
    };
    this.mediafilepicker = new Mediafilepicker();
    this.mediafilepicker.openFilePicker(options);
    this.mediafilepicker.on("getFiles", this.onGetFile);
  }

  onGetFile = (res: EventData) => {
    // The usage with a callback seems to require this
    // https://github.com/NativeScript/nativescript-angular/issues/1102
    this.zone.run(() => {
      const results = res.object.get("results");
      if (results.length) {
        const csv_file = results[0];
        this.setFileName.emit(csv_file.file.replace(/^.*[\\\/]/, ""));
        File.fromPath(csv_file.file)
          .readText()
          .then(data => {
            this.importText = data;
            const secrets = this.importerService.textToSecrets(this.importText);
            this.setImportableSecrets.emit(secrets);
          });
      }
    });
  };

  /*
   ** Table display
   */
  toggleAll() {
    const secrets = this.secrets.slice(0);
    secrets.forEach((secret, index: number) => {
      const updatedSecret = Object.assign({}, secret);
      if (secret.importable) {
        updatedSecret.doImport = !this.toggleAllState;
        secrets[index] = updatedSecret;
      }
    });
    this.toggleAllState = !this.toggleAllState;
    this.setImportableSecrets.emit(secrets);
    this.toggleAllUseMinus = false;
  }

  toggleImport(i: number) {
    const secrets = this.secrets.slice(0);
    if (secrets[i].importable) {
      const updatedSecret = Object.assign({}, secrets[i]);
      updatedSecret.doImport = !updatedSecret.doImport;
      secrets[i] = updatedSecret;
      this.setImportableSecrets.emit(secrets);
      this.setToggleAllState(secrets);
    }
  }

  setToggleAllState(secrets: ImportableSecret[]) {
    let secretsToImport = 0;
    secrets.forEach((secret, index: number) => {
      if (secret.doImport) {
        secretsToImport++;
      }
    });
    if (secrets.length !== secretsToImport && secretsToImport !== 0) {
      this.toggleAllState = true;
      this.toggleAllUseMinus = true;
    } else {
      this.toggleAllUseMinus = false;
      if (secretsToImport === 0) {
        this.toggleAllState = false;
      }
    }
  }

  trackSecret(index: number, secret: { id: number }) {
    return secret ? secret.id : undefined;
  }
}
