import { TestBed } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { AuthInterceptor } from "./auth.interceptor";
import { HTTP_INTERCEPTORS, HttpClient } from "@angular/common/http";
import { StoreModule, Store } from "@ngrx/store";
import { reducers } from "../account/account.reducer";
import { of } from "rxjs";

describe(`AuthHttpInterceptor`, () => {
  let httpMock: HttpTestingController;
  let interceptor: AuthInterceptor;
  let http: HttpClient;
  let store: any;
  const token = "abc";

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot({}),
        StoreModule.forFeature("account", reducers)
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        }
      ]
    });

    store = TestBed.get(Store);
    spyOn(store, "pipe").and.returnValue(of(token));
    interceptor = TestBed.get(HTTP_INTERCEPTORS);
    httpMock = TestBed.get(HttpTestingController);
    http = TestBed.get(HttpClient);
  });

  it("Add auth token when it exists", () => {
    http.get("/data").subscribe(response => {
      expect(response).toBeTruthy();
    });

    interceptor.token = token;
    const req = httpMock.expectOne(
      r =>
        r.headers.has("Authorization") &&
        r.headers.get("Authorization") === "token " + token
    );
    expect(req.request.method).toEqual("GET");

    req.flush({ hello: "world" });
    httpMock.verify();
  });
});
