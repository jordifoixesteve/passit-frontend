import { IData } from "./api.interfaces";

export interface INewSecret {
  name?: string;
  /** Classification of secret, for example a website or note. */
  type?: string;
  /** Unencrypted data for less sensative infomation. */
  visible_data?: IData;
  /** Encrypted data */
  secrets?: IData;
  /** Group id. undefined indicates it belongs to a user instead */
  group_id?: number | null;
  groups?: number[];
}

export interface ISecret extends INewSecret {
  id: number;
}
