export interface IUser {
  id: number;
  email: string;
  public_key: string;
  private_key: string;
  client_salt: string;
  opt_in_error_reporting: boolean;
}

export interface ILoginResonse {
  user: IUser;
  token: string;
}

export interface IPublicAuthResponse {
  client_salt: string;
  id: number;
}

export interface IUserPublicKey {
  id: number;
  public_key: string;
}

export interface ICreateUser {
  email: string;
  password: string;
  public_key: string;
  private_key: string;
  private_key_backup: string;
  client_salt: string;
  opt_in_error_reporting?: boolean;
}

export interface ICreateGroup {
  key_ciphertext: string;
  name: string;
  private_key_ciphertext: string;
  public_key: string;
  slug: string;
}

export interface ICreateGroupResp {
  id: number;
  name: string;
  slug: string;
}

export interface IGroupUser {
  id: number;
  user: number;
  group: number;
  /** Does this user have write access to the group. */
  is_group_admin: boolean;
  /** If true, this user has not accepted the invite yet */
  is_invite_pending: boolean;
}

export interface IGroup {
  id: number;
  name: string;
  public_key: string;
  /** A Many to Many through table */
  groupuser_set: IGroupUser[];
  // AES key for current user
  my_key_ciphertext: string;
  /** A copy of the RSA private key that can be decrypted using AES key */
  my_private_key_ciphertext: string;
}

export interface ICreateGroupUser {
  key_ciphertext: string;
  private_key_ciphertext: string;
  user: number;
  user_email: string;
  is_group_admin?: boolean;
}

export interface IContact {
    id: number;
    email: string;
    first_name: string;
    last_name: string;
}

export interface IData {
    [propName: string]: string | undefined;
}

export interface ICreateSecretThrough {
    /** Encrypted data */
    data: IData;
    /** Group id. Null indicates it belongs to a user instead */
    group?: number | null;
    key_ciphertext: string;
}

export interface ICreateSecretThroughGroup extends ICreateSecretThrough {
    group: number;
}

export interface ICreateSecret {
    name: string;
    /** Classification of secret, for example a website or note. */
    type?: string;
    /** Unencrypted data for less sensative infomation. */
    data: IData;
    /** Encrypted data */
    secret_through_set: ICreateSecretThrough[];
}

export interface ISecretThrough {
    id: number;
    /** Encrypted data */
    data: IData;
    /** Group id. Null indicates it belongs to a user instead */
    group: number | null;
    key_ciphertext: string;
    public_key: string;
    is_mine: boolean;
}

export interface ISecretThroughGroup extends ISecretThrough {
    group: number;
}

export interface ISecret {
    id: number;
    name: string;
    /** Classification of secret, for example a website or note. */
    type: string;
    /** Unencrypted data for less sensative infomation. */
    data: IData;
    /** Encrypted data */
    secret_through_set: ISecretThrough[];
}

export interface IUpdateSecret {
    name?: string;
    /** Classification of secret, for example a website or note. */
    type?: string;
    /** Unencrypted data for less sensative infomation. */
    data?: IData;
}

export interface IUpdateSecretThrough extends Partial<ICreateSecretThrough> {
  id: number;
}

export interface IUpdateSecretWithThroughs extends IUpdateSecret {
  secret_through_set: IUpdateSecretThrough[];
}

export interface IChangePasswordSecretThrough extends ISecretThrough {
    hash: string;
}

export interface IChangePasswordGroupUser extends IGroupUser {
  hash: string;
  private_key_ciphertext: string;
  key_ciphertext: string;
}

export interface IChangePassword {
  old_password: string;
  user: {
    password: string;
    public_key: string;
    private_key: string;
    private_key_backup?: string;
    client_salt: string;
  };
  secret_through_set: IChangePasswordSecretThrough[];
  group_user_set: IChangePasswordGroupUser[];
}

export interface IConf {
  IS_DEMO: boolean;
  IS_PRIVATE_ORG_MODE: boolean;
  ENVIRONMENT: string | null;
  RAVEN_DSN: string;
}
