import { storiesOf, moduleMetadata } from "@storybook/angular";
import { withKnobs, number, boolean, select } from "@storybook/addon-knobs";
import { RouterTestingModule } from "@angular/router/testing";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { HttpClientModule } from "@angular/common/http";
import { InlineSVGModule } from "ng-inline-svg";

import { PopupItemComponent } from "~/app/extension/popup/popup-item/popup-item.component";
import { PopupComponent } from "~/app/extension/popup/popup.component";
import { SharedModule } from "~/app/shared/shared.module";
import { PopupOnboardingComponent } from "~/app/extension/popup/popup-onboarding/popup-onboarding.component";

storiesOf("Extension", module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [
        RouterTestingModule,
        ScrollingModule,
        SharedModule,
        InlineSVGModule.forRoot(),
        HttpClientModule
      ],
      declarations: [
        PopupComponent,
        PopupItemComponent,
        PopupOnboardingComponent
      ]
    })
  )
  .add("Popup", () => ({
    component: PopupComponent,
    props: {
      secrets: [
        {
          name: "Google",
          id: 1,
          type: "website",
          data: {
            url: select(
              "Website",
              { "Has website": "www.google.com", "No website": "" },
              "www.google.com"
            ),
            username: "gman"
          },
          secret_through_set: [
            {
              data: {}
            }
          ]
        }
      ],
      matchedSecrets: [],
      search: "",
      selectedSecret: number("selectedSecret", 2),
      usernameCopied: true,
      passwordCopied: true,
      totalSecretsCount: number("totalSecretsCount", 1),
      popupFirstTimeLoadingComplete: boolean("firstTimeLoadingComplete", true)
    }
  }))
  .add("Onboarding", () => ({
    component: PopupOnboardingComponent,
    props: {
      secret: {
        data: {
          url: select(
            "Website",
            { "Has website": "www.google.com", "No website": "" },
            "www.google.com"
          )
        }
      },
      totalSecretsCount: number("totalSecretsCount", 1)
    }
  }));
