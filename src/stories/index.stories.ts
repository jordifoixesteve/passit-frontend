import { InlineSVGModule } from "ng-inline-svg";
import { HttpClientModule } from "@angular/common/http";
import { storiesOf, moduleMetadata } from "@storybook/angular";
import { action } from "@storybook/addon-actions";
import { boolean, select, withKnobs } from "@storybook/addon-knobs";
import { StoreModule } from "@ngrx/store";
import { NgrxFormsModule } from "ngrx-forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { BackupCodeComponent } from "../app/account/backup-code/backup-code.component";
import { ResetPasswordComponent } from "../app/account/reset-password/reset-password.component";
import { MarketingFrameComponent } from "../app/account/marketing-frame/marketing-frame.component";
import { LoginComponent } from "../app/account/login/login.component";
import { SharedModule } from "../app/shared/shared.module";
import { ProgressIndicatorModule } from "../app/progress-indicator/progress-indicator.module";
import * as fromResetPassword from "../app/account/reset-password/reset-password.reducer";
import * as fromLogin from "../app/account/login/login.reducer";
import * as fromChangePassword from "../app/account/change-password/change-password.reducer";
import * as fromDelete from "../app/account/delete/delete.reducer";
import * as fromSetPassword from "../app/account/reset-password/set-password/set-password.reducer";
import * as fromResetPasswordVerify from "../app/account/reset-password/reset-password-verify/reset-password-verify.reducer";
import { RouterTestingModule } from "@angular/router/testing";
import { ChangePasswordComponent } from "../app/account/change-password";
import { PasswordInputComponent } from "../app/account/change-password/password-input/password-input.component";
import { ManageBackupCodeComponent } from "../app/account/manage-backup-code/manage-backup-code.component";
import { BackupCodePdfService } from "../app/account/backup-code-pdf.service";
import { SetPasswordComponent } from "../app/account/reset-password/set-password/set-password.component";
import { ResetPasswordVerifyComponent } from "../app/account/reset-password/reset-password-verify/reset-password-verify.component";
import { DownloadBackupCodeComponent } from "../app/account/manage-backup-code/download-backup-code/download-backup-code.component";
import { ServerSelectComponent } from "../app/account/shared/server-select.component";
import { DeleteComponent } from "~/app/account/delete/delete.component";

storiesOf("Account", module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [
        InlineSVGModule.forRoot(),
        HttpClientModule,
        NgrxFormsModule,
        SharedModule,
        RouterTestingModule,
        ProgressIndicatorModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({})
      ],
      declarations: [
        MarketingFrameComponent,
        PasswordInputComponent,
        ServerSelectComponent,
        DownloadBackupCodeComponent
      ]
    })
  )
  .add("Backup Code", () => ({
    component: BackupCodeComponent,
    props: {
      code: "CBVDCH4G23MJ46FXJBR8GDX4WKBF97VR"
    }
  }))
  .add("Reset Password", () => ({
    component: ResetPasswordComponent,
    props: {
      isExtension: boolean("isExtension", true),
      hasStarted: boolean("hasStarted", false),
      hasFinished: boolean("hasFinished", false),
      form: fromResetPassword.initialFormState
    }
  }))
  .add("Reset Password Verify", () => {
    const isSubmitted = boolean("Is submitted", false);
    const isValid = boolean("Is valid", false);
    const form = {
      ...fromResetPasswordVerify.initialResetPasswordVerifyFormState,
      isSubmitted,
      isValid
    };

    return {
      component: ResetPasswordVerifyComponent,
      props: {
        form,
        verify: action("code entered"),
        showScanner: boolean("show scanner", false),
        hasStarted: boolean("has started", false),
        badCodeError: boolean("bad code error", false)
          ? "Passit does not recognize this backup code."
          : ""
      }
    };
  })
  .add("Set Password", () => ({
    component: SetPasswordComponent,
    props: {
      form: fromSetPassword.initialState.form,
      hasStarted: boolean("has started", false),
    }
  }))
  .add("Marketing Frame", () => ({
    component: MarketingFrameComponent
  }))
  .add("Form Label", () => {
    const defaultFormLabelState = "active";
    const formLabelControls = {
      Default: defaultFormLabelState,
      Inactive: "inactive",
      Complete: "complete"
    };
    // any type is, I think, because storybook is 4.0 but types aren't
    const formLabelControl: any = select(
      "Label State",
      formLabelControls,
      defaultFormLabelState
    );

    const defaultLabelTextState = "Password";
    const labelTextControls = {
      Short: defaultLabelTextState,
      Medium: "Verify Email Address",
      Long: "Download PDF with New Backup Code",
      "Extra Long":
        "Download PDF with New Backup Code So That You Can Forget Your Password As Long As You Don't Forget Where You Kept This",
      Weird: "🍫 ⋆ 🍭  🎀  𝒫𝒶𝓈𝓈𝒾𝓉 𝓁𝑒𝓉'𝓈 𝑔𝑒𝓉 𝓈𝒶𝒻𝑒 𝓉💙𝒹𝒶𝓎  🎀  🍭 ⋆ 🍫"
    };
    // any type is, I think, because storybook is 4.0 but types aren't
    const labelTextControl: any = select(
      "Label Text",
      labelTextControls,
      defaultLabelTextState
    );

    return {
      template: `
        <h3>Basic label</h3>

        <app-form-label
          for="nothing"
          [isLarge]="${boolean("Is Large", true)}"
          [isInactive]="${formLabelControl === "inactive" ? true : false}"
          [isComplete]="${formLabelControl === "complete" ? true : false}"
        >
          ${labelTextControl}
        </app-form-label>

        <h3>Testing out consecutive fields (these are wrapped in large form-field divs)</h3>

        <div class="form-field form-field--large">
          <app-form-label
            for="nothing"
            [isLarge]="true"
            [isInactive]="${formLabelControl === "inactive" ? true : false}"
            [isComplete]="${formLabelControl === "complete" ? true : false}"
          >
            ${labelTextControl}
          </app-form-label>
        </div>

        <div class="form-field form-field--large">
          <app-form-label
            for="nothing"
            [isLarge]="true"
            [isInactive]="${formLabelControl === "inactive" ? true : false}"
            [isComplete]="${formLabelControl === "complete" ? true : false}"
          >
            ${labelTextControl}
          </app-form-label>
        </div>

        <div class="form-field form-field--large">
          <app-form-label
            for="nothing"
            [isLarge]="true"
            [isInactive]="${formLabelControl === "inactive" ? true : false}"
            [isComplete]="${formLabelControl === "complete" ? true : false}"
          >
            ${labelTextControl}
          </app-form-label>
        </div>

        <h3>Testing out a field with input and link to the right</h3>

        <div style="max-width: 500px; margin: 0 auto;">
          <div class="form-field form-field--large">
            <div class="form-field__wrapper-for-link">
              <app-form-label for="foo" [isLarge]="true">
                ${labelTextControl}
              </app-form-label>
              <app-text-link class="form-field__label-link" tabindex="8">
                <span class="u-visible--all-but-small">Forgot? </span>Recover&nbsp;your&nbsp;account
              </app-text-link>
            </div>
            <input
              id="foo"
              type="password"
              class="form-field__input"
              tabindex="3"
            >
          </div>
        </div>
      `
    };
  })
  .add("Login", () => ({
    component: LoginComponent,
    props: {
      form: fromLogin.initialState.form
    }
  }))
  .add("Change Password", () => ({
    component: ChangePasswordComponent,
    props: {
      form: fromChangePassword.initialState.form,
      hasStarted: boolean("hasStarted", true),
      hasFinished: boolean("hasFinished", true)
    }
  }))
  .add("Password Input", () => ({
    component: PasswordInputComponent,
    props: {
      showConfirm: fromChangePassword.initialState.form.controls.showConfirm,
      newPassword: fromChangePassword.initialState.form.controls.newPassword,
      newPasswordConfirm:
        fromChangePassword.initialState.form.controls.newPasswordConfirm,
      errors: fromChangePassword.initialState.form.errors
    }
  }))
  .add("Download Backup Code", () => {
    const backupCodeToPdf = new BackupCodePdfService();
    return {
      component: DownloadBackupCodeComponent,
      props: {
        hasFinished: boolean("hasFinished", true),
        downloadPDF: () =>
          backupCodeToPdf.download("CBVDCH4G23MJ46FXJBR8GDX4WKBF97VR")
      }
    };
  })
  .add("Manage Backup Code", () => {
    const backupCodeToPdf = new BackupCodePdfService();
    return {
      component: ManageBackupCodeComponent,
      props: {
        form: fromChangePassword.initialState.form,
        hasStarted: boolean("hasStarted", false),
        hasFinished: boolean("hasFinished", false),
        code: "CBVDCH4G23MJ46FXJBR8GDX4WKBF97VR",
        downloadPDF: () => backupCodeToPdf.download(this.code)
      }
    };
  })
  .add("Delete Account", () => ({
    component: DeleteComponent,
    props: {
      form: fromDelete.initialState.form,
      hasStarted: boolean("hasStarted", false)
    }
  }));
