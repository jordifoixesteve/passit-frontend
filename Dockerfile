FROM node:10-alpine

RUN mkdir /dist
WORKDIR /dist

COPY package.json /dist/package.json
COPY package-lock.json /dist/package-lock.json
RUN npm i

VOLUME /dist/node_modules
ENV PATH /dist/node_modules/.bin:$PATH
COPY . /dist/

ENTRYPOINT ["npm", "run"]
